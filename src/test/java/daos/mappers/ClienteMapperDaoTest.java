package daos.mappers;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import daos.dtos.ClienteDaoDto;
import daos.dtos.FiltroClienteDaoIn;
import daos.enums.TipoSexo;
import db.dtos.ClienteEntity;
import db.dtos.FiltroClienteDbIn;

public class ClienteMapperDaoTest {

	@Test
	public void nulosDeClienteDaoInToFiltroClienteDbIn() {

		final ClienteMapperDao clienteMapperDao = new ClienteMapperDao();

		final FiltroClienteDaoIn filtroClienteDaoIn = llenarFiltroClienteDaoIn();

		final FiltroClienteDbIn filtroClienteDbIn = clienteMapperDao
				.mapFiltroClienteDaoInToFiltroClienteDbIn(filtroClienteDaoIn);

		Assert.assertNotNull(filtroClienteDbIn.getNumeroDeCliente());
		Assert.assertNotNull(filtroClienteDbIn.getSexo());

		assertTrue(filtroClienteDbIn.getNumeroDeCliente() == 1);
		assertTrue(filtroClienteDbIn.getSexo() == 'F');

	}

	@Test
	public void validarSalidaDeClienteOut() {

		final ClienteMapperDao clienteMapperDao = new ClienteMapperDao();

		final ClienteEntity clienteEntity = llenarClienteEntity();

		final ClienteDaoDto clienteDaoDto = clienteMapperDao
				.mapClienteEntityToClienteDaoDto(clienteEntity);

		Assert.assertNotNull(clienteDaoDto.getNombre());
		Assert.assertNotNull(clienteDaoDto.getTelefono());
		Assert.assertNotNull(clienteDaoDto.getEdad());
		Assert.assertNotNull(clienteDaoDto.getNumeroDeCliente());
		Assert.assertNotNull(clienteDaoDto.getSexo());

		assertTrue(clienteDaoDto.getEdad() == 18);
		assertTrue(clienteDaoDto.getNombre() == "pepito");
		assertTrue(clienteDaoDto.getNumeroDeCliente() == 123);
		assertTrue(clienteDaoDto.getSexo() == TipoSexo.MASCULINO);
		assertTrue(clienteDaoDto.getTelefono() == "53535353");

	}

	@Test
	public void validarEntradaClienteDaoDtoToClienteEntity(){

		final ClienteMapperDao clienteMapperDao = new ClienteMapperDao();

		final ClienteDaoDto clienteDaoDto = llenarClienteDaoDto();

		final ClienteEntity clienteEntity = clienteMapperDao
				.mapClienteDaoDtoToClienteEntity(clienteDaoDto);

		Assert.assertNotNull(clienteEntity.getNombre());
		Assert.assertNotNull(clienteEntity.getTelefono());
		Assert.assertNotNull(clienteEntity.getEdad());
		Assert.assertNotNull(clienteEntity.getNumeroDeCliente());
		Assert.assertNotNull(clienteEntity.getSexo());

		assertTrue(clienteEntity.getEdad() == 18);
		assertTrue(clienteEntity.getNombre() == "pepito");
		assertTrue(clienteEntity.getNumeroDeCliente() == 123);
		assertTrue(clienteEntity.getSexo() == 'M');
		assertTrue(clienteEntity.getTelefono() == "53535353");


	}

	@Test
	public void validarEntradaClienteDaoDtoToClienteEntityNull() {

		final ClienteMapperDao clienteMapperDao = new ClienteMapperDao();

		final ClienteDaoDto clienteDaoDto = null;

		final ClienteEntity clienteEntity = clienteMapperDao
				.mapClienteDaoDtoToClienteEntity(clienteDaoDto);

		Assert.assertNotNull(clienteEntity);

	}

	private FiltroClienteDaoIn llenarFiltroClienteDaoIn(){

		final FiltroClienteDaoIn filtroClienteDaoIn = new FiltroClienteDaoIn();

		filtroClienteDaoIn.setNumeroCliente(1);
		filtroClienteDaoIn.setSexo(TipoSexo.FEMENINO);

		return filtroClienteDaoIn;


	}

	private ClienteEntity llenarClienteEntity(){

		final ClienteEntity clienteEntity = new ClienteEntity();

		clienteEntity.setEdad(18);
		clienteEntity.setNombre("pepito");
		clienteEntity.setNumeroDeCliente(123);
		clienteEntity.setSexo('M');
		clienteEntity.setTelefono("53535353");

		return clienteEntity;

	}

	private ClienteDaoDto llenarClienteDaoDto() {

		final ClienteDaoDto clienteDaoDto = new ClienteDaoDto();

		clienteDaoDto.setEdad(18);
		clienteDaoDto.setNombre("pepito");
		clienteDaoDto.setNumeroDeCliente(123);
		clienteDaoDto.setSexo(TipoSexo.MASCULINO);
		clienteDaoDto.setTelefono("53535353");

		return clienteDaoDto;

	}
}
