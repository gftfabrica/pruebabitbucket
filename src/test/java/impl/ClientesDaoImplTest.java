package impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import daos.dtos.ClienteDaoIn;
import daos.dtos.FiltroClienteDaoIn;
import daos.enums.TipoSexo;
import daos.impl.ClientesDaoImpl;
import db.ClientesDb;
import db.dtos.ClienteDbOut;
import db.dtos.ClienteEntity;
import db.dtos.FiltroClienteDbIn;

@RunWith(MockitoJUnitRunner.class)
public class ClientesDaoImplTest {

	@Mock
	private ClientesDb clientesDb;

	@InjectMocks
	private ClientesDaoImpl clientesDaoImpl = new ClientesDaoImpl();

	@Before
	public void setUp() {

	}

	@Test
	public void verificarLlamadaDeConsultarCliente() {
		/**
		 * llamada del dao a db, simula la llamada a la base de datos
		 */

		ClienteDbOut clienteDbOut = new ClienteDbOut();
		ClienteEntity clienteEntity = new ClienteEntity();
		clienteEntity.setEdad(18);
		clienteEntity.setNombre("pepito");
		clienteEntity.setNumeroDeCliente(123);
		clienteEntity.setSexo('M');
		clienteEntity.setTelefono("12345678");
		clienteDbOut.setClienteEntity(clienteEntity);

		FiltroClienteDbIn filtroClienteDbIn = new FiltroClienteDbIn();
		filtroClienteDbIn.setNumeroDeCliente(123);
		filtroClienteDbIn.setSexo('F');

		clienteDbOut.setClienteEntity(clienteEntity);

		Mockito.when(clientesDb.consultarCliente(filtroClienteDbIn))
		.thenReturn(clienteDbOut);

		FiltroClienteDaoIn filtroClienteDaoIn = new FiltroClienteDaoIn();
		filtroClienteDaoIn.setNumeroCliente(123);
		filtroClienteDaoIn.setSexo(TipoSexo.FEMENINO);

		clientesDaoImpl.consultarCliente(filtroClienteDaoIn);

		Mockito.verify(clientesDb).consultarCliente(filtroClienteDbIn);

	}


	@Test
	public void verificarLlamadaDeGuardarCliente() {
		clientesDaoImpl.guardarCliente(new ClienteDaoIn());

		Mockito.verify(clientesDb).guardarCliente(new ClienteEntity());

	}


}
