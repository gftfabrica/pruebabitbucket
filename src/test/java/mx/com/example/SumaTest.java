package mx.com.example;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

public class SumaTest {

	@Test
	public void sumaPositivos() {
		System.out.println("Sumando dos números positivos ...");
		final Suma S = new Suma(2, 3);
		assertTrue(S.sumar() == 5);
	}

	@Test
	public void sumaNegativos() {
		System.out.println("Sumando dos números negativos ...");
		final Suma S = new Suma(-2, -3);
		assertTrue(S.sumar() == -5);
	}

	@Test
	public void sumaPositivoNegativo() {
		System.out.println("Sumando un número positivo y un número negativo ...");
		final Suma S = new Suma(2, -3);
		assertTrue(S.sumar() == -1);
	}

	@Test
	public void sumaNgativoPositivo() {
		System.out.println("Sumando un número negativo y un número positivo ...");
		final Suma S = new Suma(-3, 10);
		assertTrue(S.sumar() == 7);
	}

	@Test
	public void noSuma() {
		System.out.println("no es una suma");
		final Suma R = new Suma(10, 3);
		assertTrue(R.restar() == 7);

	}

	@Test
	public void validarNulos() {

		/* final Integer cero = 0; */

		final Suma suma = new Suma(null, null);
		final Integer sumaCero = suma.sumar();

		System.out.println("es nulo");
		Assert.assertNotNull(sumaCero);
		Assert.assertEquals(new Integer(0), sumaCero);

	}


}