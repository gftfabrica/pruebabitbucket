package mx.com.example;

public class Suma {

	private Integer numero1;
	private Integer numero2;

	public int getNumero1() {
		return numero1;
	}

	public void setNumero1(final int numero1) {
		this.numero1 = numero1;
	}

	public int getNumero2() {
		return numero2;
	}

	public void setNumero2(final int numero2) {
		this.numero2 = numero2;
	}

	public Suma(final Integer num1, final Integer num2) {
		numero1 = num1;
		numero2 = num2;
	}

	public Integer sumar() {

		Integer totalSuma = new Integer(0);

		if (numero1 != null && numero2 != null) {

			totalSuma = numero1 + numero2;

		}

		return totalSuma;

	}

	public int restar() {
		return numero1 - numero2;

	}

}
/*
 * Integer totalSuma = new Integer(0);
 * 
 * if (numero1 != null && Integer.valueOf(numero2) != null) {
 * 
 * totalSuma = numero1 + numero2;
 * 
 * }
 */
