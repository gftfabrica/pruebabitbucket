package daos.mappers;

import daos.dtos.ClienteDaoDto;
import daos.dtos.ClienteDaoOut;
import daos.dtos.FiltroClienteDaoIn;
import daos.utils.converters.TipoSexoConverter;
import db.dtos.ClienteEntity;
import db.dtos.FiltroClienteDbIn;

public class ClienteMapperDao {

	public FiltroClienteDbIn mapFiltroClienteDaoInToFiltroClienteDbIn(
			final FiltroClienteDaoIn from) {

		final FiltroClienteDbIn to = new FiltroClienteDbIn();


		if (from != null) {

			to.setNumeroDeCliente(from.getNumeroCliente());
			if (from.getSexo() != null) {

				to.setSexo(from.getSexo().getSexo());
			}
		}

		return to;

	}

	public ClienteEntity mapClienteDaoDtoToClienteEntity(
			final ClienteDaoDto from) {

		final ClienteEntity to = new ClienteEntity();

		if (from != null) {

			final ClienteDaoOut clienteDaoOut = new ClienteDaoOut();

			to.setEdad(from.getEdad());
			to.setNombre(from.getNombre());
			to.setNumeroDeCliente(from.getNumeroDeCliente());
			to.setSexo(from.getSexo().getSexo());
			to.setTelefono(from.getTelefono());

			clienteDaoOut.setClienteDaoDto(from);
		}

		return to;
	}

	public ClienteDaoDto mapClienteEntityToClienteDaoDto(
			final ClienteEntity from) {

		final ClienteDaoDto to = new ClienteDaoDto();

		to.setEdad(from.getEdad());
		to.setNombre(from.getNombre());
		to.setNumeroDeCliente(from.getNumeroDeCliente());
		to.setSexo(TipoSexoConverter.convertirCharToEnumTipoSexo(from.getSexo()));
		to.setTelefono(from.getTelefono());

		return to;

	};

}
