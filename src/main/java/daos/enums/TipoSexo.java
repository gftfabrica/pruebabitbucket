package daos.enums;

public enum TipoSexo {

	MASCULINO('M'), FEMENINO('F');
	
	private Character sexo;


	TipoSexo(final Character key){
		sexo=key;

	}

	public Character getSexo() {
		return sexo;
	}

}
