package daos;

import daos.dtos.ClienteDaoIn;
import daos.dtos.ClienteDaoOut;
import daos.dtos.FiltroClienteDaoIn;
import daos.dtos.GuardaClienteDaoOut;

public interface ClienteDao {

	ClienteDaoOut consultarCliente(FiltroClienteDaoIn filtroClienteDaoIn);

	GuardaClienteDaoOut guardarCliente(ClienteDaoIn clienteDaoIn);


}
