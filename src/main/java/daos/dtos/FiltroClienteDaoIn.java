package daos.dtos;

import daos.enums.TipoSexo;

public class FiltroClienteDaoIn {

	private Integer numeroDeCliente;
	private TipoSexo sexo;

	public Integer getNumeroCliente() {
		return numeroDeCliente;
	}

	public void setNumeroCliente(final Integer numeroCliente) {
		numeroDeCliente = numeroCliente;
	}

	public TipoSexo getSexo() {
		return sexo;
	}

	public void setSexo(final TipoSexo sexo) {
		this.sexo = sexo;
	}

}
