package daos.dtos;

import daos.enums.TipoSexo;

public class ClienteDaoDto {

	private Integer numeroDeCliente;
	private String nombre;
	private TipoSexo sexo;
	private Integer edad;
	private String telefono;



	public String getNombre() {
		return nombre;
	}

	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(final Integer edad) {
		this.edad = edad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(final String telefono) {
		this.telefono = telefono;
	}

	public TipoSexo getSexo() {
		return sexo;
	}

	public void setSexo(final TipoSexo sexo) {
		this.sexo = sexo;
	}

	public Integer getNumeroDeCliente() {
		return numeroDeCliente;
	}

	public void setNumeroDeCliente(final Integer numeroDeCliente) {
		this.numeroDeCliente = numeroDeCliente;
	}

}
