package daos.dtos;

public class ClienteDaoIn {

	private ClienteDaoDto cliente;

	public ClienteDaoDto getCliente() {
		return cliente;
	}

	public void setCliente(final ClienteDaoDto cliente) {
		this.cliente = cliente;
	}

}
