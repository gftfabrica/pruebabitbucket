package daos.dtos;

public class GuardaClienteDaoOut {
	
	private Boolean guardado;

	public Boolean getGuardado() {
		return guardado;
	}

	public void setGuardado(Boolean guardado) {
		this.guardado = guardado;
	}

}
