package daos.impl;

import daos.ClienteDao;
import daos.dtos.ClienteDaoDto;
import daos.dtos.ClienteDaoIn;
import daos.dtos.ClienteDaoOut;
import daos.dtos.FiltroClienteDaoIn;
import daos.dtos.GuardaClienteDaoOut;
import daos.mappers.ClienteMapperDao;
import db.ClientesDb;
import db.dtos.ClienteDbOut;
import db.dtos.ClienteEntity;
import db.dtos.FiltroClienteDbIn;
import db.impl.ClientesDbImpl;



public class ClientesDaoImpl implements ClienteDao {

	private ClientesDb clientesDb = new ClientesDbImpl();

	private  ClienteMapperDao clienteMapperDao = new ClienteMapperDao();


	public ClientesDb getClientesDbImpl() {

		return clientesDb;
	}

	public ClienteDaoOut consultarCliente(
			final FiltroClienteDaoIn filtroClienteDaoIn) {

		ClienteDaoOut clienteDaoOut = new ClienteDaoOut();

		FiltroClienteDbIn filtroClienteDbIn = clienteMapperDao
				.mapFiltroClienteDaoInToFiltroClienteDbIn(filtroClienteDaoIn);

		ClienteDbOut clienteDbOut = clientesDb
				.consultarCliente(filtroClienteDbIn);

		ClienteDaoDto clienteDaoDto = clienteMapperDao
				.mapClienteEntityToClienteDaoDto(clienteDbOut
						.getClienteEntity());

		clienteDaoOut.setClienteDaoDto(clienteDaoDto);

		return clienteDaoOut;

	}

	public GuardaClienteDaoOut guardarCliente(final ClienteDaoIn clienteDaoIn) {

		final GuardaClienteDaoOut guardaClienteDaoOut = new GuardaClienteDaoOut();

		final ClienteEntity clienteEntity = clienteMapperDao
				.mapClienteDaoDtoToClienteEntity(clienteDaoIn.getCliente());

		clientesDb.guardarCliente(clienteEntity);

		guardaClienteDaoOut.setGuardado(true);

		return guardaClienteDaoOut;
	}



}
