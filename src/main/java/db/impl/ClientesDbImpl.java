package db.impl;

import db.ClientesDb;
import db.dtos.ClienteDbOut;
import db.dtos.ClienteEntity;
import db.dtos.FiltroClienteDbIn;

public class ClientesDbImpl implements ClientesDb {

	public ClienteDbOut consultarCliente(FiltroClienteDbIn filtroClienteDbIn) {

		ClienteDbOut clienteDbOut = new ClienteDbOut();

		ClienteEntity clienteEntity = new ClienteEntity();

		clienteEntity.setNombre("pedro");
		clienteEntity.setEdad(18);
		clienteEntity.setNumeroDeCliente(123);
		clienteEntity.setSexo('F');
		clienteEntity.setTelefono("53535353");

		clienteDbOut.setClienteEntity(clienteEntity);

		System.out.println("ejecutando Consulta");

		return clienteDbOut;
	}

	public void guardarCliente(final ClienteEntity clientedb) {
		System.out.println("guardar cliente");

	}

}
