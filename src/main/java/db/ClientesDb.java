package db;

import db.dtos.ClienteDbOut;
import db.dtos.ClienteEntity;
import db.dtos.FiltroClienteDbIn;

public interface ClientesDb {

	ClienteDbOut consultarCliente(FiltroClienteDbIn filtroClienteDbIn);

	void guardarCliente(ClienteEntity clientedb);

}
