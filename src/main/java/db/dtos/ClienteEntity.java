package db.dtos;

public class ClienteEntity {

	private Integer numeroDeCliente;
	private String nombre;
	private Character sexo;
	private Integer edad;
	private String telefono;


	public String getNombre() {
		return nombre;
	}

	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	public Character getSexo() {
		return sexo;
	}

	public void setSexo(final Character sexo) {
		this.sexo = sexo;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(final Integer edad) {
		this.edad = edad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(final String telefono) {
		this.telefono = telefono;
	}

	public Integer getNumeroDeCliente() {
		return numeroDeCliente;
	}

	public void setNumeroDeCliente(final Integer numeroDeCliente) {
		this.numeroDeCliente = numeroDeCliente;
	}

}
