package db.dtos;


public class FiltroClienteDbIn {

	private Integer numeroDeCliente;
	private Character sexo;

	public Integer getNumeroDeCliente() {
		return numeroDeCliente;
	}

	public void setNumeroDeCliente(final Integer numeroDeCliente) {
		this.numeroDeCliente = numeroDeCliente;
	}

	public Character getSexo() {
		return sexo;
	}

	public void setSexo(final Character sexo) {
		this.sexo = sexo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (numeroDeCliente == null ? 0 : numeroDeCliente.hashCode());
		result = prime * result + (sexo == null ? 0 : sexo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FiltroClienteDbIn other = (FiltroClienteDbIn) obj;
		if (numeroDeCliente == null) {
			if (other.numeroDeCliente != null) {
				return false;
			}
		} else if (!numeroDeCliente.equals(other.numeroDeCliente)) {
			return false;
		}
		if (sexo == null) {
			if (other.sexo != null) {
				return false;
			}
		} else if (!sexo.equals(other.sexo)) {
			return false;
		}
		return true;
	}





}
